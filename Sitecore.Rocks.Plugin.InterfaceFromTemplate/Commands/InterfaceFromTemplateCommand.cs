using System;
using System.CodeDom.Compiler;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Xml.Linq;
using Sitecore.Rocks.Commands;
using Sitecore.Rocks.ContentTrees;
using Sitecore.Rocks.ContentTrees.Items;
using Sitecore.Rocks.Data;
using Sitecore.Rocks.Extensions.DataServiceExtensions;

// ReSharper disable UnusedMember.Global

namespace Sitecore.Rocks.Plugin.InterfaceFromTemplate.Commands
{
    internal static class TemplateIDs
    {
        public static readonly ItemId Template = new ItemId(new Guid("{AB86861A-6030-46C5-B394-E8F99E8B87DB}"));
        public static readonly ItemId TemplateField = new ItemId(new Guid("{455A3E98-A627-4B40-8035-E683A0331AC7}"));
        public static readonly ItemId TemplateFolder = new ItemId(new Guid("{0437FEE2-44C9-46A6-ABE9-28858D9FEE8C}"));
        public static readonly ItemId TemplateSection = new ItemId(new Guid("{E269FBB5-3750-427A-9149-7AA950B49301}"));
    }

    //[Command(Submenu = ClipboardSubmenu.Name)]
    [Command]
    public class InterfaceFromTemplateCommand : CommandBase
    {
        private CodeDomProvider CodeDomProvider { get; }

        public InterfaceFromTemplateCommand()
        {
            CodeDomProvider = CodeDomProvider.CreateProvider("C#");

            Text = "Generate interface from template";
            //Group = "code generation";
            Group = "Templates";
            SortingValue = 10;
        }

        public override bool CanExecute(object parameter)
        {
            var context = parameter as ContentTreeContext;
            if (context == null)
            {
                return false;
            }

            var selectedItems = context.SelectedItems?.ToArray() ?? new BaseTreeViewItem[0];
            if (selectedItems.Count() != 1)
            {
                return false;
            }

            var item = selectedItems.FirstOrDefault() as ItemTreeViewItem;
            if (item == null)
            {
                return false;
            }

            if (!item.IsTemplate)
            {
                return false;
            }

            return true;
        }

        public override void Execute(object parameter)
        {
            var context = parameter as ContentTreeContext;

            var item = context?.SelectedItems.FirstOrDefault() as ItemTreeViewItem;
            if (item == null)
            {
                return;
            }

            item.ItemUri.Site.DataService.GetTemplateXml(item.ItemUri, true,
                                                         templateXml => Clipboard.SetText(WriteTemplate(templateXml)));
        }

        /// <summary>
        /// <remarks>
        /// This will only generate code withvalid identifiers. It will not check that the generated code can compile.
        /// Sitecore will happily have two fields with the same name exist on a single template. This will result in generated code that does not compile.</remarks>
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        private string WriteTemplate(XDocument template)
        {
            if (template?.Root == null)
            {
                return "";
            }

            string fieldsCodeStructureName = GetSafeName((string)template.Root.Attribute("name")) + "Fields";
            const string fieldNamePostfix = "_FieldName";

            var sections = template
                .Root
                .Elements("section")
                .OrderBy(section => (string)section.Attribute("name"))
                .Select(section => new
                {
                    Section = section,
                    Fields = section
                                .Descendants("field")
                                .Where(field => (int)field.Attribute("system") == 0)
                                .Select(field => new
                                {
                                    Element = field,
                                    FieldId = (string)field.Attribute("id"),
                                    Name = (string)field.Attribute("name"),
                                    FieldType = (string)field.Attribute("type"),
                                    SafeName = GetSafeName((string)field.Attribute("name")),
                                })
                                .Select(field => new
                                {
                                    FieldElement = field,
                                    FieldIdCommentLine = $@"// Field ID ""{field.FieldId}""",
                                    FieldTypeCommentLine = $@"// Field type ""{field.FieldType}""",
                                    SitecoreFieldAttributeLine = $@"[SitecoreField({fieldsCodeStructureName}.{field.SafeName}{fieldNamePostfix})]",
                                    FieldPropertyLine = $@"{GetFieldType(field.FieldType)} {field.SafeName}" + " { get; set; }",
                                    FieldIdLine = $@"public static readonly Guid {field.SafeName} = new Guid(""{(Guid)field.Element.Attribute("id"):B}"");",
                                    FieldNameLine = $@"public const string {field.SafeName}{fieldNamePostfix} = ""{field.Name}"";"
                                })
                                .ToArray()
                })
                .Where(section => section.Fields.Any())
                .ToArray();

            var interfaceCode = new StringBuilder()
                .WriteCodeLine($@"[SitecoreType(TemplateId = ""{(string)template.Root.Attribute("id")}"")]", 0)
                .WriteCodeLine($@"public interface I{GetSafeName((string)template.Root.Attribute("name"))} : IGlassBase", 0)
                .WriteCodeLine("{", 0);

            var structCode = new StringBuilder()
                .WriteCodeLine($@"public static class {fieldsCodeStructureName}", 0)
                .WriteCodeLine("{", 0);

            for (int i = 0; i < sections.Length; i++)
            {
                var section = sections[i];

                interfaceCode = interfaceCode
                    .AppendLineIf(i > 0)
                    .WriteCodeLine($@"#region Section - {(string)section.Section.Attribute("name")} - Start", 1)
                    .AppendLine();

                for (int fidx = 0; fidx < section.Fields.Length; fidx++)
                {
                    var field = section.Fields[fidx];

                    interfaceCode
                        .AppendLineIf(fidx > 0)
                        .WriteCodeLine(field.FieldIdCommentLine, 1)
                        .WriteCodeLine(field.FieldTypeCommentLine, 1)
                        .WriteCodeLine(field.SitecoreFieldAttributeLine, 1)
                        .WriteCodeLine(field.FieldPropertyLine, 1);

                    structCode = structCode
                        .AppendLineIf(i + fidx > 0)
                        .WriteCodeLine(field.FieldIdLine, 1)
                        .WriteCodeLine(field.FieldNameLine, 1);
                }

                interfaceCode = interfaceCode
                    .AppendLine()
                    .WriteCodeLine($@"#endregion Section - {(string)section.Section.Attribute("name")} - End", 1);
            }

            interfaceCode = interfaceCode.WriteCodeLine("}", 0);

            structCode = structCode.WriteCodeLine("}", 0);

            return interfaceCode + "\n" + structCode;
        }

        private string GetSafeName(string name)
        {
            var safeNameParts = name
                .Replace('_', ' ')
                .Trim()
                .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => char.ToUpper(s[0]) + s.Substring(1));

            string safeName = string.Join("_", safeNameParts);

            var regex = new Regex(@"[^\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}\p{Nl}\p{Mn}\p{Mc}\p{Cf}\p{Pc}\p{Lm}]");

            safeName = regex.Replace(safeName, string.Empty);
            if (safeName[0] != '_' && (!char.IsLetter(safeName, 0) || !CodeDomProvider.IsValidIdentifier(safeName)))
            {
                safeName = string.Concat("_", safeName);
            }

            var result = safeName;

            return result;
        }

        /// <summary>
        /// Inspired by TDS Glass Mapper tt file and reworked a bit
        /// Note that this will not map custom field types and it will also fail if the items for the field types in the core database are renamed
        /// </summary>
        /// <param name="sitecoreFieldType"></param>
        /// <returns></returns>
        private string GetFieldType(string sitecoreFieldType)
        {
            string unknownType = "Sitecore-link-field-type-insert-own-custom-type-here";

            switch (sitecoreFieldType.ToLower())
            {
                case "tristate":
                    return "TriState";

                case "checkbox":
                    return "bool";

                case "date":
                case "datetime":
                    return "DateTime";

                case "number":
                    return "float";

                case "integer":
                    return "int";

                case "treelist":
                case "treelistex":
                case "treelist descriptive":
                case "queryenabledtreelist":
                case "checklist":
                case "multilist":
                    return $"IEnumerable<{unknownType}>";

                case "grouped droplink":
                case "droplink":
                case "lookup":
                case "droptree":
                case "reference":
                case "tree":
                    return unknownType;

                case "file":
                    return "File";

                case "image":
                    return "Image";

                case "rich text":
                case "html":
                    return "string";

                case "general link":
                    return "Link";

                case "single-line text":
                case "multi-line text":
                case "frame":
                case "text":
                case "memo":
                case "droplist":
                case "grouped droplist":
                case "valuelookup":
                    return "string";

                default:
                    return unknownType;
            }
        }
    }

    internal static class Extensions
    {
        internal static StringBuilder WriteCodeLine(this StringBuilder sb, string line, int lvl, string tab = "\t")
            => sb.Append(string.Join("", Enumerable.Repeat(tab, lvl)))
                 .AppendLine(line);

        internal static StringBuilder AppendLineIf(this StringBuilder sb, bool predicate, string line = null)
            => sb.AppendLineIf(() => predicate, line);

        internal static StringBuilder AppendLineIf(this StringBuilder sb, Func<bool> predicate, string line = null)
            => predicate()
                ? sb.AppendLine(line)
                : sb;
    }
}