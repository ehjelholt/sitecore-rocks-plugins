$solutionPath = Get-Location
$nugetexePath = (Join-Path $solutionPath ".nuget\nuget.exe")
$outputPath = (Join-Path $env:USERPROFILE "AppData\Local\Sitecore\Sitecore.Rocks.VisualStudio\Plugins\TDCInterfaceExtractor")
$project = (Join-Path $solutionPath "Sitecore.Rocks.Plugin.InterfaceFromTemplate\Sitecore.Rocks.Plugin.InterfaceFromTemplate.csproj")

if (Test-Path "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe") {
    $msbuild = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe"
} 
elseif (Test-Path "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe") 
{
    $msbuild = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe"
} 
elseif (Test-Path "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe")
{
    $msbuild = "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe"
}
else 
{
    $msbuild = "C:\Program Files (x86)\MSBuild\12.0\Bin\MSBuild.exe"
}

Write-Host "Restoring nuget packages"
&$nugetexePath restore $project -PackagesDirectory (Join-Path $solutionPath "packages")

Write-Host "Building project"
&$msbuild $project /p:Configuration=Debug /p:OutputPath=$outputPath
